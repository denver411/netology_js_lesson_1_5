'use strict'

//Задача № 1
//Функция
function guaranteeOrder(guaranteePeriod) {
    //убрал проверку количества лет (уже есть в default кейсе в switch)
    //расчет стоимости гарантии
    var guaranteeCost;
    switch (guaranteePeriod) {
        case 1:
            guaranteeCost = 1250;
            break;
        case 2:
            guaranteeCost = 2300;
            break;
        default:
            guaranteeCost = 0;
    }
    return guaranteeCost; //убрал возврат количества лет в гарантии, так как передается снаружи и уже и так известен
}
//Вывод результата
var guaranteePeriod = 1;
console.log(`Дополнительное гарантийное обслуживание: ${guaranteeOrder(guaranteePeriod)} Q`);

//Задача № 2
//Функция расчета стоимости гравировки
function engravingCost(text) {
    //Проверка на наличие текста для гравировки
    if (!text) {
        return 0;
    }
    text = deleteTooManyGaps(text);
    //Расчет стоимости
    var cost = 11;
    var wordsNumber = text.split(' ').length;
    return (wordsNumber * cost);
}

//Функция для удаления лишних пробелов
function deleteTooManyGaps(text) {
    //Удаление пробелов перед текстом для гравировки
    var startString = /^ +/;
    if (startString.test(text)) {
        text = text.replace(startString, '');
    }
    //Удаление пробелов после текста для гравировки
    var endString = / +$/;
    if (endString.test(text)) {
        text = text.replace(endString, '');
    }
    //Удаление пробелов в середине текста для гравировки
    var middleString = / +/g;
    if (middleString.test(text)) {
        text = text.replace(middleString, ' ');
    }
    return text;
}

//Аргументы функции
var textForEngraving = 't ttext text';

//Вывод результата
console.log(`Подарочная упаковка и гравировка: ${engravingCost(textForEngraving)} Q`);

//Задача № 3

//Функция
function deliveryCost(hasDelivery, area) {
    if (!hasDelivery) {
        return 0;
    } else {
        switch (area) {
            case 'Луна':
                return 150; //убрал break, так как есть return
            case 'Крабовидная туманность':
                return 250; //убрал break, так как есть return
            case 'Галактика Туманность Андромеды':
                return 550; //убрал break, так как есть return
            case 'Туманность Ориона':
                return 600; //убрал break, так как есть return
            case 'Звезда смерти':
                return 'договорная цена'; //убрал break, так как есть return
            default:
                return NaN;
        }
    }
}

//Аргументы функции
var delivery = true,
    deliveryArea = 'Крабовидная туманность';

//Вывод результата
if (deliveryCost(delivery, deliveryArea) === 0) {
    console.log('Доставка не требуется');
} else if (isNaN(deliveryCost(delivery, deliveryArea))) {
    console.log('Ошибка при расчете стоимости доставки');
} else {
    console.log(`Стоимость доставки: ${deliveryCost(delivery, deliveryArea)} Q`);
}

//Задача № 4

//Функция
function orderTotalCost(orderCost, guaranteePeriod, engravingText, hasDelivery, deliveryToArea) {
    var guarantee = guaranteeOrder(guaranteePeriod);
    var engraving = engravingCost(engravingText);
    var delivery = deliveryCost(hasDelivery, deliveryToArea);
    // посчитается неверно, если delivery будет NaN
    // добавил проверку на NaN
    var orderTotalSum;
    if (!delivery) {
        orderTotalSum = orderCost + guarantee + engraving;
    } else {
    orderTotalSum = orderCost + guarantee + engraving + delivery;
}
    return [orderTotalSum, guarantee, engraving, delivery];
}

//Аргументы функции
var orderCost = 1000,
    guaranteePeriod = 2,
    engravingText = 'text t text',
    deliveryIsOrdered = true,
    deliveryToArea = 'Туманн ость Ориона';
//проверка периода гарантии на соответствие диапазону
if (guaranteePeriod != 1 && guaranteePeriod != 2) {
    guaranteePeriod = 0;
}

//Вывод результата
var result = orderTotalCost(orderCost, guaranteePeriod, engravingText, deliveryIsOrdered, deliveryToArea);
//проверка доставки
if (result[3] === 0) {
    deliveryToArea = 'не требуется';
} else if (!result[3]) {
    result[3] = 0;
    deliveryToArea = 'не производится';
} else if (result[3] === 'договорная цена') {
    deliveryToArea = deliveryToArea + ' производится по договорной цене';
    result[3] = 0;
}

console.log(`Общая стоимость заказа: ${result[0]} Q. 
    Из них ${result[1]} Q за гарантийное обслуживание на ${guaranteePeriod} год/года. 
    Гравировка на сумму ${result[2]} Q. 
    Доставка в область ${deliveryToArea}: ${result[3]} Q.`);